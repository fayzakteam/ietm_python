from django.shortcuts import render
from django.views.generic.edit import View
from django.contrib.auth.mixins import LoginRequiredMixin
from users.models import File, Video
from itertools import chain

# Create your views here.


class HomeView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'elec/elec_home.html')

# ! SUBSYSTEMS
class ChargingSysView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'elec/charging.html')
class StartingSysView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'elec/starting.html')
class LightingSysView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'elec/lighting.html')
class ChargingAccessoryView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'elec/accessory.html')

# ! DATA VIEWS
# Charging Sys
class ChargingEMERView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        crumb = 'EMER'
        subsystem = 'Charging System'
        link = '{{% url "elec:charging_emer" %}}'
        # datalist = File.objects.filter(subsystem="CHARGING", content_type="EMERS")
        datalist=''
        return render(request, 'elec/data.html', {'crumb':crumb, 'sub':subsystem, 'datalist':datalist})


# ! Video VIEWS
# Charging
class ChargingVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(video_type="CHARGING")
        return render(request, 'elec/videos/charging.html', {'datalist':datalist})
# lighting
class LightingVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(video_type="LIGHTING")
        return render(request, 'elec/videos/lighting.html', {'datalist':datalist})
# starting
class StartingVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(video_type="STARTING")
        return render(request, 'elec/videos/starting.html', {'datalist':datalist})
# accesory
class AccessoryVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(video_type="ACCESSORY")
        return render(request, 'elec/videos/accessory.html', {'datalist':datalist})