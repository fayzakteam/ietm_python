from django.urls import path
from .views import (HomeView,
ChargingAccessoryView, ChargingSysView, LightingSysView, StartingSysView,
ChargingEMERView,
AccessoryVideoView, StartingVideoView, LightingVideoView, ChargingVideoView)


app_name = 'elec'
urlpatterns = [
    path('', HomeView.as_view(), name='home'),

    path('charging/', ChargingSysView.as_view(), name='charging'),
    path('lighting/', LightingSysView.as_view(), name='lighting'),
    path('starting/', StartingSysView.as_view(), name='starting'),
    path('accessory/', ChargingAccessoryView.as_view(), name='accessory'),
    path('charging/videos', ChargingVideoView.as_view(), name='c_vid'),
    path('lighting/videos', LightingVideoView.as_view(), name='l_vid'),
    path('starting/videos', StartingVideoView.as_view(), name='s_vid'),
    path('accessory/videos', AccessoryVideoView.as_view(), name='a_vid'),

    path('charging/emers/', ChargingEMERView.as_view(), name='charging_emer'),



]