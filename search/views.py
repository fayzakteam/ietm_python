from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger
from django.core.paginator import Paginator
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView 
from django.shortcuts import render
from django.db.models import Q
from operator import attrgetter
from itertools import chain

from users.models import Video
from users.models import File


class SearchView(LoginRequiredMixin, ListView):
    FILTER_MAP = {
        "technical":["TECHNICAL LITERATURE - USER HANDBOOK", "TECHNICAL LITERATURE - EMERS", "TECHNICAL LITERATURE - TECHNICAL MANUAL", "TECHNICAL LITERATURE - ISPL", "TECHNICAL LITERATURE - MRLS"], 
        "ppt":["PPT - AUTO", "PPT - ELEC", "PPT - GCE", "CBT"], 
        "faults":["DEFECT REPORT", "FAULT COMPENDIUM"]
        }

    def get(self, request, *args, **kwargs):
        search_query = self.request.GET.get("q", None)
        search_filters = self.request.GET.getlist("filters", [])
        search_tags = search_query.strip().split(" ") if search_query else []
        query_string = request.get_full_path().split("?")[1] if search_query else ""
        if "&page" in query_string:
            query_string = query_string.split("&page")[0] 
        page = self.request.GET.get("page", 1)

        search_results = File.objects.none()
        video_search_results = []
        file_search_results = []
        

        if search_query:
            if "all" in search_filters:
                video_search_results = Video.objects.filter(Q(name__icontains=search_query) | 
                                                         Q(descriptions__icontains=search_query) |
                                                         Q(tags__name__in=search_tags))
                file_search_results = File.objects.filter(Q(name__icontains=search_query) | 
                                                         Q(tags__name__in=search_tags))
            else:
                if "videos" in search_filters:
                    video_search_results = Video.objects.filter(Q(name__icontains=search_query) | 
                                                         Q(descriptions__icontains=search_query) |
                                                         Q(tags__name__in=search_tags))

                file_search_results =  File.objects.filter((Q(name__icontains=search_query) |
                                        Q(tags__name__in=search_tags)),
                                        Q(content_type__in=self._get_combined_file_filters(search_filters)))

            search_results = sorted(chain(video_search_results, file_search_results),key=attrgetter('name'))


            print(search_query)
            print(search_filters)
            print(search_tags)

        paginator = Paginator(search_results, 10)
        try:
            search_results = paginator.page(page)
        except PageNotAnInteger:
            search_results = paginator.page(1)
        except EmptyPage:
            search_results = paginator.page(paginator.num_pages)

        return render(
            self.request,
            "search/search.html",
            {
                "search_query": search_query,
                "search_filters": search_filters,
                "search_results": search_results,
                "query_string": query_string
            },
        )

    def _get_combined_file_filters(self, search_filters):
        requested_file_filters = []
        file_filters = self.FILTER_MAP.keys()

        for filter in search_filters:
            if filter in file_filters:
                requested_file_filters += self.FILTER_MAP[filter]
        
        return requested_file_filters


