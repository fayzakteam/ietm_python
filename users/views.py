from django.views.generic.edit import View
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import (authenticate, login, 
logout, get_user_model)
from users.forms import (LoginForm)
from users.models import File, Video, FileObject
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import update_session_auth_hash
from django.views.generic import ListView, DeleteView
from django.contrib.auth.models import User
from django_filters.views import FilterView
from django.db.models import Q
import os
from pathlib import Path
from django.conf import settings
# Create your views here.


#! User Authencation Views

class LoginView(View):


    def get(self,request,*args,**kwargs):
        if request.user.is_authenticated:
            return redirect('users:home')
        else:
            form = LoginForm()
            return render(request, 'users/users_login.html', {'form': form})

    def post(self,request,*args,**kwargs):
        if request.method == 'POST':
            form = LoginForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data.get('username')
                password = form.cleaned_data.get('password')
                user = authenticate(username=username, password=password)
                login(request, user)
                return redirect('users:home')
        else:
            form = LoginForm()
        return render(request, 'users/users_login.html', {'form': form})

class LogoutView(LoginRequiredMixin,View):
    def get(self, request, *args, **kwargs):
        logout(request)
        return render(request, 'users/users_login.html')

class LandingView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('users:home')
        else:
            return render(request, 'users/users_login.html')

#! Common Views

class HomeView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/users_home.html')

class AboutView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/users_about.html')

class RsvView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/rsv.html')

class RsvTransmitterView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/rsv-transmitter.html')

class RsvSdpView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/rsv-sdp.html')

class RsvCiuView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/rsv-ciu.html')

class RsvDisplaysystemView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/rsv-displaysystem.html')


class RsvIffView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/rsv-iff.html')

class RsvGalleryView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/rsv-gallery.html')


class RsvFlowView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/rsv-flow-diagram.html')

class EsvView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/esv.html')

class EsvFlowView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/esv-flow-diagram.html')


# ! SCHEMATIC VIEW
class SchematicsView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/schematics/main.html')
class AutoSchemView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        schem_auto = File.objects.filter(content_type='SCHEMATIC DIAGRAM')
        return render(request, 'users/schematics/auto.html', {'schem_auto':schem_auto})
class GceSchemView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        schem_gce = File.objects.filter(content_type='SCHEMATIC DIAGRAM - GCE')
        return render(request, 'users/schematics/gce.html', {'schem_gce':schem_gce})
class ElecSchemView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        schem_elec = File.objects.filter(content_type='SCHEMATIC DIAGRAM - ELEC')
        return render(request, 'users/schematics/elec.html', {'schem_elec':schem_elec})

# ! TECHNICAL LITERATURE 

class TechnicalLiteratureView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        tl = File.objects.filter(content_type='TECHNICAL LITERATURE')
        return render(request, 'users/tech_lit/main.html', {'tl':tl})
class PrecisView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/tech_lit/precis.html')


# ! EMERs VIEW

class EMERsView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        emer = File.objects.filter(content_type='EMER')
        return render(request, 'users/emers/main.html', {'emer':emer})
class SMTView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/emers/smt.html')

# ! CBTS/PPTS

class CBTPPTView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/cbt_ppt/main.html')
class CBTMainView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/cbt_ppt/cbt_main.html')
class PPTMainView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        ppt = File.objects.filter(content_type='PPT')
        return render(request, 'users/cbt_ppt/ppt_main.html', {'ppt':ppt})

# CBTs
class CBTGCEView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        gce_path = os.path.join(settings.BASE_DIR,Path('static/CBT/GCE/gec2/Gce_start.exe'))
        os.system(gce_path)
        return render(request, 'users/cbt_ppt/cbt_main.html')   
    def post(self, request, *args, **kwargs):
        gce_path = os.path.join(settings.BASE_DIR,'static/CBT/GCE/gec2/Gce_start.exe')
        os.system(gce_path)
        return render(request, 'users/cbt_ppt/cbt_main.html')
class CBTGCEiView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        gce_path = os.path.join(settings.BASE_DIR,Path('static/CBT/GCE/gec1/ELECTRONICS.exe'))
        os.system(gce_path)
        return render(request, 'users/cbt_ppt/cbt_main.html')   
    def post(self, request, *args, **kwargs):
        gce_path = os.path.join(settings.BASE_DIR,'static/CBT/GCE/gec1/ELECTRONICS.exe')
        os.system(gce_path)
        return render(request, 'users/cbt_ppt/cbt_main.html')
class CBTAutoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        gce_path = os.path.join(settings.BASE_DIR,Path('static/CBT/Automotive/Auto/MCEME01_2.exe'))
        os.system(gce_path)
        return render(request, 'users/cbt_ppt/cbt_main.html')   
    def post(self, request, *args, **kwargs):
        gce_path = os.path.join(settings.BASE_DIR,'static/CBT/Automotive/Auto/MCEME01_2.exe')
        os.system(gce_path)
        return render(request, 'users/cbt_ppt/cbt_main.html')

# PPTs
class PPTGCEView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        ppt_gce = File.objects.filter(content_type='PPT - GCE')
        return render(request, 'users/cbt_ppt/ppt_gce.html', {'ppt_gce':ppt_gce})
class PPTAutoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        ppt_auto = File.objects.filter(content_type='PPT - AUTO')
        return render(request, 'users/cbt_ppt/ppt_auto.html', {'ppt_auto':ppt_auto})
class PPTElecView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        ppt_elec = File.objects.filter(content_type='PPT - ELEC')
        return render(request, 'users/cbt_ppt/ppt_elec.html', {'ppt_elec':ppt_elec})


class GCELessonsView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/cbt_ppt/ppt_gce_lessons.html')

# ! DEFECT REPORT

class DefectView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        defect_reports = File.objects.filter(content_type='DEFECT REPORT')
        return render(request, 'users/defect_reports.html', {'defect_reports':defect_reports})
# ! MISC VIEW

class MiscView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'users/misc.html')
class MiscFilesView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        misc = File.objects.filter(content_type='MISC')
        return render(request, 'users/misc_files.html', {'misc':misc})
class MiscVideosView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        misc = Video.objects.filter(video_type='MISC')
        return render(request, 'users/misc_videos.html', {'misc':misc})


# ! Search Views

class SearchListView(LoginRequiredMixin, ListView):
    template_name = 'users/search.html'
    def get_queryset(self):
        query = str(self.request.GET.get('q'))
        if query is not None:
            queryset = {
            'files': File.objects.filter(name__icontains=query),
            'videos': Video.objects.filter(Q(name__icontains=query) | Q(descriptions__icontains=query)),
            }
            return queryset
        
        return Guide.objects.none()
    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['query'] = str(self.request.GET.get('q'))
        return context
