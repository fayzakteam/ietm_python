from django.contrib import admin
from users.models import File, Video
from taggit.admin import Tag

# Register your models here.

@admin.register(File)
class FileAdmin(admin.ModelAdmin):
    list_display = ['__str__']
    save_on_top = True

@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    list_display = ['__str__']
    save_on_top = True

admin.site.unregister(Tag)