# Generated by Django 3.1.7 on 2021-03-02 16:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20210228_1818'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='content_type',
            field=models.CharField(choices=[('TECHNICAL LITERATURE - USER HANDBOOK', 'TECHNICAL LITERATURE - USER HANDBOOK'), ('TECHNICAL LITERATURE - EMERS', 'TECHNICAL LITERATURE - EMERS'), ('TECHNICAL LITERATURE - TECHNICAL MANUAL', 'TECHNICAL LITERATURE - TECHNICAL MANUAL'), ('TECHNICAL LITERATURE - ISPL', 'TECHNICAL LITERATURE - ISPL'), ('TECHNICAL LITERATURE - MRLS', 'TECHNICAL LITERATURE - MRLS'), ('PROCEDURES', 'PROCEDURES'), ('SMT', 'SMT'), ('PERFOMANCE REPORT', 'PERFOMANCE REPORT'), ('TM1 &TM2', 'TM1 &TM2'), ('PPT - AUTO', 'PPT - AUTO'), ('PPT - ELEC', 'PPT - ELEC'), ('PPT - GCE', 'PPT - GCE'), ('CBT', 'CBT'), ('FAULT COMPENDIUM', 'FAULT COMPENDIUM'), ('RSV - TECHNICAL MANUAL', 'RSV - TECHNICAL MANUAL'), ('RSV - PPT', 'RSV - PPT'), ('ESV - TECHNICAL MANUAL', 'ESV - TECHNICAL MANUAL'), ('ESV - PPT', 'ESV - PPT'), ('TROUBLESHOOTING', 'TROUBLESHOOTING'), ('MISC', 'MISC'), ('SCHEMATIC DIAGRAM - AUTO', 'SCHEMATIC DIAGRAM - AUTO'), ('SCHEMATIC DIAGRAM - ELEC', 'SCHEMATIC DIAGRAM - ELEC'), ('SCHEMATIC DIAGRAM - GCE', 'SCHEMATIC DIAGRAM - GCE'), ('EMER', 'EMER'), ('DEFECT REPORT', 'DEFECT REPORT')], max_length=120),
        ),
    ]
