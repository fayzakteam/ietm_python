from django.urls import path

from .views import (HomeView,
AutoSchemView,GceSchemView,ElecSchemView,SchematicsView,
TechnicalLiteratureView,PrecisView,
EMERsView,SMTView,
CBTPPTView,CBTGCEView,CBTGCEiView,CBTAutoView,CBTMainView,PPTMainView,
PPTGCEView,PPTElecView,PPTAutoView,GCELessonsView,
DefectView,
MiscView,MiscFilesView, MiscVideosView,
SearchListView)


app_name = 'users'
urlpatterns = [
    path('', HomeView.as_view(), name='home'),

    path('schematics/', SchematicsView.as_view(), name='schem'),
    path('schematics/auto/', AutoSchemView.as_view(), name='sc_auto'),
    path('schematics/gce/', GceSchemView.as_view(), name='sc_gce'),
    path('schematics/elec/', ElecSchemView.as_view(), name='sc_elec'),

    path('tech_lit/', TechnicalLiteratureView.as_view(), name='technical_literature'),
    path('tech_lit/precis/', PrecisView.as_view(), name='precis'),
    
    path('emers/', EMERsView.as_view(), name='emer'),
    path('emers/smt/', SMTView.as_view(), name='smt'),

    path('cbt/', CBTPPTView.as_view(), name='cbt'),
    path('cbts/all', CBTMainView.as_view(), name='cbt_main'),
    path('ppts/all', PPTMainView.as_view(), name='ppt_main'),
    path('cbt/gce/', CBTGCEView.as_view(), name='cbt_gce'),
    path('cbt/gce-electronics/', CBTGCEiView.as_view(), name='cbt_gce_elec'),
    path('cbt/auto/', CBTAutoView.as_view(), name='cbt_auto'),
    path('ppt/auto/', PPTAutoView.as_view(), name='ppt_auto'),
    path('ppt/elec/', PPTElecView.as_view(), name='ppt_elec'),
    path('ppt/gce/', PPTGCEView.as_view(), name='ppt_gce'),
    path('ppt/gce/lessons/', GCELessonsView.as_view(), name='ppt_gce_lessons'),

    path('defects/', DefectView.as_view(), name='defect_report'),

    path('misc/', MiscView.as_view(), name='misc'),
    path('misc/files', MiscFilesView.as_view(), name='misc_files'),
    path('misc/videos', MiscVideosView.as_view(), name='misc_videos'),

    path('results/', SearchListView.as_view(), name='results')
]