from django.db import models
import os
from users.validators import validate_general_file, validate_video_file, validate_icon_file

from taggit.managers import TaggableManager 

# Create your models here.


CONTENT = (
    ('TECHNICAL LITERATURE - USER HANDBOOK', 'TECHNICAL LITERATURE - USER HANDBOOK'),
    ('TECHNICAL LITERATURE - EMERS', 'TECHNICAL LITERATURE - EMERS'),
    ('TECHNICAL LITERATURE - TECHNICAL MANUAL', 'TECHNICAL LITERATURE - TECHNICAL MANUAL'),
    ('TECHNICAL LITERATURE - ISPL', 'TECHNICAL LITERATURE - ISPL'),
    ('TECHNICAL LITERATURE - MRLS', 'TECHNICAL LITERATURE - MRLS'),
    ('PROCEDURES', 'PROCEDURES'),
    ('SMT', 'SMT'),
    ('PERFOMANCE REPORT', 'PERFOMANCE REPORT'),
    ('TM1 &TM2', 'TM1 &TM2'),
    ('PPT - AUTO', 'PPT - AUTO'),
    ('PPT - ELEC', 'PPT - ELEC'),
    ('PPT - GCE', 'PPT - GCE'),
    ('CBT', 'CBT'),
    ('FAULT COMPENDIUM', 'FAULT COMPENDIUM'),
    ('RSV - TECHNICAL MANUAL', 'RSV - TECHNICAL MANUAL'),
    ('RSV - PPT', 'RSV - PPT'),
    ('ESV - TECHNICAL MANUAL', 'ESV - TECHNICAL MANUAL'),
    ('ESV - PPT', 'ESV - PPT'),
    ('TROUBLESHOOTING', 'TROUBLESHOOTING'),
    ('MISC', 'MISC'),
    ('SCHEMATIC DIAGRAM - AUTO', 'SCHEMATIC DIAGRAM - AUTO'),
    ('SCHEMATIC DIAGRAM - ELEC', 'SCHEMATIC DIAGRAM - ELEC'),
    ('SCHEMATIC DIAGRAM - GCE', 'SCHEMATIC DIAGRAM - GCE'),
    ('EMER', 'EMER'),
    ('DEFECT REPORT', 'DEFECT REPORT'),
)

VIDEO = (
    ('ALG', 'ALG'),
    ('GCE', 'GCE'),
    ('CBRN', 'CBRN'),
    ('FFS', 'FFS'),
    ('CHARGING', 'CHARGING'),
    ('LIGHTING', 'LIGHTING'),
    ('STARTING', 'STARTING'),
    ('ENGINE', 'ENGINE'),
    ('COOLING', 'COOLING'),
    ('FUEL', 'FUEL'),
    ('TRANSMISSION SYSTEM', 'TRANSMISSION SYSTEM'),
    ('RUNNING GEAR', 'RUNNING GEAR'),
    ('LUBRICATION', 'LUBRICATION'),
    ('PNEUMATIC', 'PNEUMATIC'),
    ('MISC', 'MISC'),
)


class File(models.Model):
    content_type = models.CharField(choices = CONTENT, max_length=120)
    name = models.CharField(max_length=120)
    icon = models.FileField(upload_to='uploads/icons',validators=[validate_icon_file])
    file = models.FileField(upload_to='uploads/files',validators=[validate_general_file])
    tags = TaggableManager()

    def __str__(self):
        return f'{self.content_type} --> {os.path.basename(self.file.name)}'

    def get_extension(self):
        return os.path.splitext(self.file.url)[1]
    
    def get_filename(self):
        _ , tail = ntpath.split(self.file.url)
        return tail


class Video(models.Model):
    video_type = models.CharField(choices = VIDEO, max_length=120)
    name = models.CharField(max_length=120)
    descriptions = models.CharField(max_length=600)
    file = models.FileField(upload_to='uploads/videos',validators=[validate_video_file])
    tags = TaggableManager()

    def __str__(self):
        return f'{self.video_type} --> {os.path.basename(self.file.name)}'

    def get_extension(self):
        return os.path.splitext(self.file.url)[1]
    
    def get_filename(self):
        _ , tail = ntpath.split(self.file.url)
        return tail

class FileObject():

    name = ''

    def __init__(self, name):
        self.name = name