import os
from django.core.exceptions import ValidationError

def validate_general_file(value):
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.pdf','.doc','.docx','.ppt','.pptx']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported filetype. Upload a valid file.')

def validate_icon_file(value):
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.ico','.png','.jpg','.jpeg']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported filetype. Upload a valid icon file.')

def validate_video_file(value):
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.mp4','.avi','.mpg','.mpeg','.flv','.vob','.mov']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported filetype. Upload a valid video file.')