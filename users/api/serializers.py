from rest_framework import serializers
from users.models import File, Video

class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = [
            'name',
        ]
class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = [
            'name',
            'descriptions'
        ]