from django.urls import path
from .views import (AxiosFileQuesryset,AxiosVideoQuesryset)


app_name = 'api'
urlpatterns = [
    path('files/', AxiosFileQuesryset.as_view(), name='files'),
    path('videos/', AxiosVideoQuesryset.as_view(), name='videos'),
]