from rest_framework.generics import ListAPIView
from django.contrib.auth.mixins import LoginRequiredMixin
from users.models import File, Video
from .serializers import (FileSerializer, VideoSerializer)

class AxiosFileQuesryset(LoginRequiredMixin, ListAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer
class AxiosVideoQuesryset(LoginRequiredMixin, ListAPIView):
    queryset = Video.objects.all()
    serializer_class = VideoSerializer