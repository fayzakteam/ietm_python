from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import (authenticate, login, logout, get_user_model)


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self,*args,**kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        
        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                raise forms.ValidationError('This user does not exist or Incorrect Password.')
            if not user.check_password(password):
                raise forms.ValidationError('Incorrect Password')
            if not user.is_active:
                raise forms.ValidationError('User is no longer active.')
        return super(LoginForm, self).clean(*args, **kwargs)



class SearchForm(forms.Form):
    query = forms.CharField(max_length=200)