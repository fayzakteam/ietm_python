from django.shortcuts import render
from django.views.generic.edit import View
from django.contrib.auth.mixins import LoginRequiredMixin
from users.models import File, Video
from itertools import chain

# Create your views here.


class HomeView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'gce/gce_home.html')

# ! SUBSYSTEMS
class GCSSysView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'gce/gcs.html')
class ALGSysView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'gce/alg.html')
class FFSSysView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'gce/ffs.html')
class CBRNView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'gce/cbrn.html')

# ! Video VIEWS
# ALG
class ALGVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(video_type="ALG")
        return render(request, 'gce/videos/alg.html', {'datalist':datalist})
# CBRN
class CBRNVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(video_type="CBRN")
        return render(request, 'gce/videos/cbrn.html', {'datalist':datalist})
# FFS
class FFSVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(video_type="FFS")
        return render(request, 'gce/videos/ffs.html', {'datalist':datalist})
# GCS
class GCSVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(video_type="GCE")
        return render(request, 'gce/videos/gcs.html', {'datalist':datalist})