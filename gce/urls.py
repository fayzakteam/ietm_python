from django.urls import path
from .views import (HomeView,
ALGSysView, GCSSysView, FFSSysView, CBRNView,
ALGVideoView,CBRNVideoView,GCSVideoView,FFSVideoView)


app_name = 'gce'
urlpatterns = [
    path('', HomeView.as_view(), name='home'),

    path('alg/', ALGSysView.as_view(), name='alg'),
    path('gcs/', GCSSysView.as_view(), name='gcs'),
    path('ffs/', FFSSysView.as_view(), name='ffs'),
    path('cbrn/', CBRNView.as_view(), name='cbrn'),

    path('alg/video/', ALGVideoView.as_view(), name='alg_videos'),
    path('cbr/video/', CBRNVideoView.as_view(), name='cbr_videos'),
    path('gcs/video/', GCSVideoView.as_view(), name='gcs_videos'),
    path('ffs/video/', FFSVideoView.as_view(), name='ffs_videos'),

]