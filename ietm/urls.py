from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static


from users.views import (LoginView, LogoutView, LandingView, 
                        AboutView, RsvView, EsvView, RsvFlowView, 
                        EsvFlowView, RsvTransmitterView, RsvGalleryView, RsvSdpView, RsvIffView, RsvCiuView, RsvDisplaysystemView)
from search.views import SearchView

admin.site.site_header = "IETM Admin"
admin.site.site_title = "IETM Admin Portal"
admin.site.index_title = "Welcome to IETM Admin Portal"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', LandingView.as_view(), name='landing'),
    path('rsv/', RsvView.as_view(), name='rsv'),
    path('rsv/transmitter', RsvTransmitterView.as_view(), name='rsv-transmitter'),
    path('rsv/gallery', RsvGalleryView.as_view(), name='rsv-gallery'),
    path('rsv/rsv-flow-diagram/', RsvFlowView.as_view(), name='rsv-flow-diagram'),
    path('rsv/sdp', RsvSdpView.as_view(), name='rsv-sdp'),
    path('rsv/iff', RsvIffView.as_view(), name='rsv-iff'),
    path('rsv/ciu', RsvCiuView.as_view(), name='rsv-ciu'),
    path('rsv/displaysystem', RsvDisplaysystemView.as_view(), name='rsv-displaysystem'),
    path('esv/', EsvView.as_view(), name='esv'),
    path('esv-flow-diagram/', EsvFlowView.as_view(), name='esv-flow-diagram'),
    path('about/', AboutView.as_view(), name='about'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('auto/', include('auto.urls', namespace="auto")),
    path('elec/', include('elec.urls', namespace="elec")),
    path('gce/', include('gce.urls', namespace="gce")),
    path('ietm/', include('users.urls', namespace="users")),
    # api urls
    path('api/', include('users.api.urls', namespace="api")),
    path('search/', SearchView.as_view(), name='search'),

]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
