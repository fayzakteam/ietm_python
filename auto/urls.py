from django.urls import path
from .views import (HomeView,
EngineView,CoolingSysView,LubricationSysView,PowerTrainView,RunningGearView,PneumaticSysView,FuelSysView,
EngineEMERView,
EngineVideoView,LubricationVideoView,CoolingVideoView,PTVideoView,RGVideoView,PneumaticVideoView,FuelVideoView)


app_name = 'auto'
urlpatterns = [
    path('', HomeView.as_view(), name='home'),

    path('engine/', EngineView.as_view(), name='engine'),
    path('cooling/', CoolingSysView.as_view(), name='cooling_system'),
    path('lubrication/', LubricationSysView.as_view(), name='lubrication_system'),
    path('power-train/', PowerTrainView.as_view(), name='power_train'),
    path('running-gear/', RunningGearView.as_view(), name='running_gear'),
    path('pneumatic/', PneumaticSysView.as_view(), name='pneumatic_system'),
    path('fuel/', FuelSysView.as_view(), name='fuel_system'),

    path('engine/videos', EngineVideoView.as_view(), name='evid'),
    path('cooling/videos', CoolingVideoView.as_view(), name='cvid'),
    path('lubrication/videos', LubricationVideoView.as_view(), name='lvid'),
    path('power-train/videos', PTVideoView.as_view(), name='ptvid'),
    path('running-gear/videos', RGVideoView.as_view(), name='rgvid'),
    path('pneumatic/videos', PneumaticVideoView.as_view(), name='pvid'),
    path('fuel/videos', FuelVideoView.as_view(), name='fvid'),

    path('engine/emers/', EngineEMERView.as_view(), name='engine_emer'),
]