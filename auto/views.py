from django.shortcuts import render
from django.views.generic.edit import View
from django.contrib.auth.mixins import LoginRequiredMixin
from users.models import File, Video
from itertools import chain
from django.db.models import Q

# Create your views here.


class HomeView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'auto/auto_home.html')

# ! SUBSYSTEMS
class EngineView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'auto/engine.html')
class CoolingSysView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'auto/cooling.html')
class LubricationSysView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'auto/lubrication.html')
class FuelSysView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'auto/fuel.html')
class PowerTrainView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'auto/power_train.html')
class RunningGearView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'auto/running_gear.html')
class PneumaticSysView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'auto/pneumatic.html')



# ! Video VIEWS
# Engine
class EngineVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(video_type="ENGINE")
        return render(request, 'auto/videos/engine.html', {'datalist':datalist})
# Cooling
class CoolingVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(video_type="COOLING")
        return render(request, 'auto/videos/cooling.html', {'datalist':datalist})
# Fuel
class FuelVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(video_type="FUEL")
        return render(request, 'auto/videos/fuel.html', {'datalist':datalist})
# Power Train
class PTVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(Q(video_type="POWER TRAIN") | Q(video_type="TRANSMISSION SYSTEM"))
        return render(request, 'auto/videos/power_train.html', {'datalist':datalist})
# Running Gear
class RGVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(video_type="RUNNING GEAR")
        return render(request, 'auto/videos/running_gear.html', {'datalist':datalist})
# Lubrication
class LubricationVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(video_type="LUBRICATION")
        return render(request, 'auto/videos/lubrication.html', {'datalist':datalist})
# Pneumatic
class PneumaticVideoView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        datalist = Video.objects.filter(video_type="PNEUMATIC")
        return render(request, 'auto/videos/pneumatic.html', {'datalist':datalist})



# ! DATA VIEWS
# Engine
class EngineEMERView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        crumb = 'EMER'
        subsystem = 'Engine'
        link = '{% url "auto:engine" %}'
        # datalist = File.objects.filter(subsystem="ENGINE", content_type="EMERS")
        datalist=''
        return render(request, 'auto/data.html', {'crumb':crumb, 'sub':subsystem, 'link':link, 'datalist':datalist})
