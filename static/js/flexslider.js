$(function() {
    $(window).load(function(){
        $('#thumbcarousel').flexslider({
          animation: "none",
          controlNav: false,
          animationLoop: false,
          slideshow: false,
          itemWidth: 80,
          itemMargin: 5,
          asNavFor: '#diagramslider'
        });
    
        $('#diagramslider').flexslider({
          animation: "fade",
          controlNav: false,
          animationLoop: false,
          slideshow: false,
          sync: "#thumbcarousel",
          start: function(slider){
            $('body').removeClass('loading');
          }
        });
    });
});