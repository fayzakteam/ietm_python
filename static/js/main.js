$(function() {
      $('.back-to-top').click(function() {
        $('html, body').animate({scrollTop: 0}, 800);
         return false;
     });

     $("#resultsAll").click(function(e){
          if($("#resultsAll").is(":checked")){
            $(".results-filter").prop('checked', false)
          }
     });

     $(".results-filter").click(function(e){
      if($(".results-filter:checked").length){
        $("#resultsAll").prop('checked', false)
      }
 });
});